# pytest coverage test

[![pipeline status](https://gitlab.com/nyker510/pytest-coverage-test/badges/master/pipeline.svg)](https://gitlab.com/nyker510/pytest-coverage-test/-/commits/master)

[![coverage report](https://gitlab.com/nyker510/pytest-coverage-test/badges/master/coverage.svg)](https://gitlab.com/nyker510/pytest-coverage-test/-/commits/master)

### Coverage の設定

Settings > CI/CD / General pipeline  から `Test coverage parsing` を設定。pytest の場合以下の通り

```
^TOTAL.+?(\d+\%)$
```

https://gitlab.com/nyker510/pytest-coverage-test/-/settings/ci_cd

### gitlab-ci.yml の設定

* --junitxml=report.xml を指定して junit 形式の xml を作成, artifacts に渡す
  * これによって test report にテスト結果が表示されるようになる: https://gitlab.com/nyker510/pytest-coverage-test/-/pipelines/330317864/test_report
* coverage xml を実行して xml を cobertura に渡す
  * これによって merge request の diff に対して、テストケースのテストが行われているかどうかが可視化される。
  * 若干反映までに時間がかかるので注意 (CI が終了してすぐには反映されないことがある。不具合などではないので注意。)
  * だた、時間がたっても反映されない場合がある。特に sub directory にパッケージなどがある場合 (ex: apps 配下にすべての py files が存在していて pytest は ./apps で実行したい時)、上手くパースが出来ない時がある。これは gitlab ci の coverage に渡された xml のパス解析が原因のよう。原因がちゃんとわかっていない。
    * https://forum.gitlab.com/t/enabling-cobertura/36918/12